package com.dy.test;

import com.dy.domain.Admin;
import com.dy.mapper.AdminMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class TestAdminMapper {
    /**
     * 测试登陆验证
     */
    @Test
    public void Test1() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
        Admin admin = adminMapper.loginVerify("E02014049", "admin123");

        System.out.println(admin);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectAll
     */
    @Test
    public void Test2() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
        ArrayList<Admin> admins = adminMapper.selectAll();

        System.out.println(admins);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectById(int id)
     */
    @Test
    public void Test3() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
        Admin admin = adminMapper.selectById(1);

        System.out.println(admin);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试通过account查询Admin
     */
    @Test
    public void Test4() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取AdminMapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
        Admin admin = adminMapper.selectByAccount("E02014049");

        System.out.println(admin);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试add
     */
    @Test
    public void Test5() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
        Admin admin = new Admin();
        admin.setAccount("U110");
        admin.setPassword("213");
        adminMapper.add(admin);

        //提交事务
        sqlSession.commit();
        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试update
     * @throws IOException
     */
    @Test
    public void Test6() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);

        //4. 执行方法
        Admin admin = new Admin();
        admin.setId(7);
        admin.setAccount("U1");

        int count = adminMapper.update(admin);
        System.out.println(count);
        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试deleteById
     * @throws IOException
     */
    @Test
    public void Test7() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取Mapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);

        //4. 执行方法

        adminMapper.deleteById(7);

        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试deleteByIds
     * @throws IOException
     */
    @Test
    public void Test8() throws IOException {
        //接收参数
        int[] ids = {8,9};

        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
        //4. 执行方法
        adminMapper.deleteByIds(ids);
        //提交事务
        sqlSession.commit();
        //5. 释放资源
        sqlSession.close();

    }

}
