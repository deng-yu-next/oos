package com.dy.test;

import com.dy.domain.Order;
import com.dy.mapper.OrderMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class TestOrderMapper {

    /**
     * 测试selectAll
     */
    @Test
    public void Test1() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        ArrayList<Order> orders = orderMapper.selectAll();

        System.out.println(orders);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectByOid(int oid);
     */
    @Test
    public void Test2() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        Order order = orderMapper.selectByOid(1);

        System.out.println(order);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectByUid(int uid);
     */
    @Test
    public void Test3() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        ArrayList<Order> order = orderMapper.selectByUid(1);

        System.out.println(order);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试add
     */
    @Test
    public void Test4() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        Order order = new Order();
        order.setUid(5);
        order.setMoney(32.1);
        order.setStatus(1);
        order.setReason("哈哈哈");
        order.setSituation("123456");
        orderMapper.add(order);

        //提交事务
        sqlSession.commit();
        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试update
     * @throws IOException
     */
    @Test
    public void Test5() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);

        //4. 执行方法
        Order order = new Order();
        order.setOid(4);
        order.setUid(1);
        order.setMoney(31);
        order.setStatus(1);
        order.setReason("哈哈");
        order.setSituation("123456");

        int count = orderMapper.update(order);
        System.out.println(count);
        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试deleteById
     * @throws IOException
     */
    @Test
    public void Test8() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取Mapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        //4. 执行方法

        orderMapper.deleteById(4);

        //提交事务
        //sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试deleteByIds
     * @throws IOException
     */
    @Test
    public void Test9() throws IOException {
        //接收参数
        int[] ids = {1};

        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        //4. 执行方法
        orderMapper.deleteByIds(ids);
        //提交事务
        //sqlSession.commit();
        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试selectByStatus(int status)
     */
    @Test
    public void Test10() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        ArrayList<Order> orders = orderMapper.selectByStatus(1);
        ArrayList<Order> order2 = orderMapper.selectByStatus(2);
        for (Order order : order2) {
            orders.add(order);
        }
        System.out.println(orders);

        //4. 释放资源
        sqlSession.close();
    }
}
