package com.dy.test;

import com.dy.domain.Dishes;
import com.dy.mapper.DishesMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class TestDishesMapper {
    /**
     * 测试selectAll
     */
    @Test
    public void Test1() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        DishesMapper dishesMapper = sqlSession.getMapper(DishesMapper.class);
        ArrayList<Dishes> dishes = dishesMapper.selectAll();

        System.out.println(dishes);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectById(int did)
     */
    @Test
    public void Test2() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        DishesMapper dishesMapper = sqlSession.getMapper(DishesMapper.class);
        Dishes dishes = dishesMapper.selectById(1);

        System.out.println(dishes);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectByDname(int did);
     */
    @Test
    public void Test3() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取AdminMapper接口的代理对象
        DishesMapper dishesMapper = sqlSession.getMapper(DishesMapper.class);
        Dishes dishes = dishesMapper.selectByDname("柠檬水");

        System.out.println(dishes);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试add
     */
    @Test
    public void Test4() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        DishesMapper dishesMapper = sqlSession.getMapper(DishesMapper.class);
        Dishes dishes = new Dishes();
        dishes.setDname("汉堡");
        dishes.setPrice(12.4);
        dishes.setDiscount(1.0);
        dishes.setImg("3.jpg");
        dishesMapper.add(dishes);

        //提交事务
        sqlSession.commit();
        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试update
     * @throws IOException
     */
    @Test
    public void Test6() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        DishesMapper dishesMapper = sqlSession.getMapper(DishesMapper.class);

        //4. 执行方法
        Dishes dishes = new Dishes();
        dishes.setDid(9);
        dishes.setDname("炸鸡");

        int count = dishesMapper.update(dishes);
        System.out.println(count);
        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试deleteById
     * @throws IOException
     */
    @Test
    public void Test7() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取Mapper接口的代理对象
        DishesMapper dishesMapper = sqlSession.getMapper(DishesMapper.class);

        //4. 执行方法

        dishesMapper.deleteById(11);

        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试deleteByIds
     * @throws IOException
     */
    @Test
    public void Test8() throws IOException {
        //接收参数
        int[] ids = {8};

        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        DishesMapper dishesMapper = sqlSession.getMapper(DishesMapper.class);
        //4. 执行方法
        dishesMapper.deleteByIds(ids);
        //提交事务
        sqlSession.commit();
        //5. 释放资源
        sqlSession.close();

    }

}
