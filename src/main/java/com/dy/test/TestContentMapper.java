package com.dy.test;

import com.dy.domain.Comment;
import com.dy.domain.Content;
import com.dy.mapper.CommentMapper;
import com.dy.mapper.ContentMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class TestContentMapper {


    /**
     * 测试selectAll
     */
    @Test
    public void Test1() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        ContentMapper contentMapper = sqlSession.getMapper(ContentMapper.class);
        ArrayList<Content> content = contentMapper.selectAll();

        System.out.println(content);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectByOid(int oid);
     */
    @Test
    public void Test2() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        ContentMapper contentMapper = sqlSession.getMapper(ContentMapper.class);
        ArrayList<Content> content = contentMapper.selectByOid(1);

        System.out.println(content);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectByDid(int did)
     */
    @Test
    public void Test3() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        ContentMapper contentMapper = sqlSession.getMapper(ContentMapper.class);
        ArrayList<Content> content = contentMapper.selectByDid(2);

        System.out.println(content);

        //4. 释放资源
        sqlSession.close();
    }


    /**
     * 测试add
     */
    @Test
    public void Test4() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        ContentMapper contentMapper = sqlSession.getMapper(ContentMapper.class);
        Content content = new Content();
        content.setOid(3);
        content.setDid(2);
        content.setNum(3);

        contentMapper.add(content);

        //提交事务
        sqlSession.commit();
        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试update
     */
    @Test
    public void Test5() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        ContentMapper contentMapper = sqlSession.getMapper(ContentMapper.class);

        //4. 执行方法
        Content content = new Content();
        content.setDid(2);
        content.setOid(1);
        content.setNum(1);

        int count = contentMapper.update(content);
        System.out.println(count);
        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试delete
     */
    @Test
    public void Test6() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取Mapper接口的代理对象
        ContentMapper contentMapper = sqlSession.getMapper(ContentMapper.class);

        //4. 执行方法

        contentMapper.delete(2,2);

        //提交事务
        //sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

}
