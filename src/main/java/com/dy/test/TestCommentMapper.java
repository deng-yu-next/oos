package com.dy.test;

import com.dy.domain.Comment;
import com.dy.domain.User;
import com.dy.mapper.CommentMapper;
import com.dy.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class TestCommentMapper {


    /**
     * 测试selectAll
     */
    @Test
    public void Test1() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        CommentMapper commentMapper = sqlSession.getMapper(CommentMapper.class);
        ArrayList<Comment> comment = commentMapper.selectAll();

        System.out.println(comment);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectByUid(int uid);
     */
    @Test
    public void Test2() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        CommentMapper commentMapper = sqlSession.getMapper(CommentMapper.class);
        ArrayList<Comment> comment = commentMapper.selectByUid(1);

        System.out.println(comment);

        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试selectByDid(int did)
     */
    @Test
    public void Test3() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        CommentMapper commentMapper = sqlSession.getMapper(CommentMapper.class);
        ArrayList<Comment> comment = commentMapper.selectByDid(2);

        System.out.println(comment);

        //4. 释放资源
        sqlSession.close();
    }


    /**
     * 测试add
     */
    @Test
    public void Test4() throws Exception {
        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        CommentMapper commentMapper = sqlSession.getMapper(CommentMapper.class);
        Comment comment = new Comment();
        comment.setUid(2);
        comment.setDid(2);
        comment.setCon("不好吃");

        commentMapper.add(comment);

        //提交事务
        sqlSession.commit();
        //4. 释放资源
        sqlSession.close();
    }

    /**
     * 测试update
     */
    @Test
    public void Test5() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3. 获取Mapper接口的代理对象
        CommentMapper commentMapper = sqlSession.getMapper(CommentMapper.class);

        //4. 执行方法
        Comment comment = new Comment();
        comment.setDid(1);
        comment.setUid(1);
        comment.setCon("送餐太慢");

        int count = commentMapper.update(comment);
        System.out.println(count);
        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

    /**
     * 测试delete
     */
    @Test
    public void Test6() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取Mapper接口的代理对象
        CommentMapper commentMapper = sqlSession.getMapper(CommentMapper.class);

        //4. 执行方法

        commentMapper.delete(2,2);

        //提交事务
        sqlSession.commit();

        //5. 释放资源
        sqlSession.close();

    }

}
