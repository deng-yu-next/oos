package com.dy.domain;

public class Admin {
    private int id; //管理员Id
    private String account; //管理员账号
    private String password; //密码
    private String status; //管理员权限状态，1是超级管理员，2是普通管理员

    public Admin(int id, String account, String password, String status) {
        this.id = id;
        this.account = account;
        this.password = password;
        this.status = status;
    }

    public Admin() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
