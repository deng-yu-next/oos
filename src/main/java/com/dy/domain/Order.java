package com.dy.domain;

public class Order {
    private int oid; //订单id
    private int uid; //用户id
    private double money;
    private int status; //状态， 表示是否申诉和处理，0是未有申诉，1是申诉但未处理，2是已处理
    private String reason; //申诉原因
    private String situation; //处理情况

    public Order(int oid, int uid, double money, int status, String reason, String situation) {
        this.oid = oid;
        this.uid = uid;
        this.money = money;
        this.status = status;
        this.reason = reason;
        this.situation = situation;
    }

    public Order() {
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    @Override
    public String toString() {
        return "Order{" +
                "oid=" + oid +
                ", uid=" + uid +
                ", money=" + money +
                ", status=" + status +
                ", reason='" + reason + '\'' +
                ", situation='" + situation + '\'' +
                '}';
    }
}
