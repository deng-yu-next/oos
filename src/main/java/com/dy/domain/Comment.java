package com.dy.domain;

public class Comment {
    private int uid;
    private int did;
    private String con; // 评论内容

    public Comment(int uid, int did, String con) {
        this.uid = uid;
        this.did = did;
        this.con = con;
    }

    public Comment() {
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "uid=" + uid +
                ", did=" + did +
                ", con='" + con + '\'' +
                '}';
    }
}
