package com.dy.domain;

public class Dishes {
    private int did;
    private String dname;
    private double price;
    private double discount;
    private String img;

    public Dishes(int did,String dname, double price, double discount, String img) {
        this.did = did;
        this.dname = dname;
        this.price = price;
        this.discount = discount;
        this.img = img;
    }

    public Dishes() {
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Dishes{" +
                "did=" + did +
                ", dname='" + dname + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                ", img='" + img + '\'' +
                '}';
    }
}
