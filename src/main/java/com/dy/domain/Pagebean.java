package com.dy.domain;

import java.util.List;


/**
 *
 * 用来分页查询
 * @param <T>
 */
public class Pagebean<T> {
    //总记录数
    private int TotalCount;
    //当前页数据
    private List<T> Rows;

    public int getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(int totalCount) {
        TotalCount = totalCount;
    }

    public List<T> getRows() {
        return Rows;
    }

    public void setRows(List<T> rows) {
        Rows = rows;
    }
}
