package com.dy.domain;

public class Content {
    int oid;
    int did;
    int num;

    public Content(int oid, int did, int num) {
        this.oid = oid;
        this.did = did;
        this.num = num;
    }

    public Content() {
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Content{" +
                "oid=" + oid +
                ", did=" + did +
                ", num=" + num +
                '}';
    }
}
