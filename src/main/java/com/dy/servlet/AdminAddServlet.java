package com.dy.servlet;

import com.alibaba.fastjson.JSONObject;
import com.dy.domain.Admin;
import com.dy.domain.User;
import com.dy.mapper.AdminMapper;
import com.dy.mapper.UserMapper;
import com.dy.utils.JsonResultUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/adminAddServlet")
public class AdminAddServlet extends HttpServlet {
    public AdminAddServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置编码类型
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String account = request.getParameter("account");
        String password = request.getParameter("password");

        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
        Admin admin = new Admin();
        admin.setAccount(account);
        admin.setPassword(password);
        adminMapper.add(admin);

        sqlSession.commit();
        sqlSession.close();

        JSONObject data = JsonResultUtil.getJson(0,1,"请求成功");
        response.getWriter().print(data);
    }
}
