package com.dy.servlet;

import com.alibaba.fastjson.JSONObject;
import com.dy.domain.Order;
import com.dy.mapper.OrderMapper;
import com.dy.utils.JsonResultUtil;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

@WebServlet("/appealManageServlet2")
public class AppealManageServlet2 extends HttpServlet {
    public AppealManageServlet2() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置编码类型
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        int uid = Integer.parseInt(request.getParameter("id"));

        //1. 加载mybatis的核心配置文件，获取 SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3.1 获取UserMapper接口的代理对象
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        ArrayList<Order> orders = orderMapper.selectByUid(uid);
        ArrayList<Order> del = new ArrayList<>();
        for (Order order : orders) {
            if (order.getStatus() == 0){
                del.add(order);
            }
        }
        orders.removeAll(del);

        //4. 释放资源
        sqlSession.close();

        JSONObject data = JsonResultUtil.getJson(0,orders,"请求成功");
        response.getWriter().print(data);
    }
}
