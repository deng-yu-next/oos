package com.dy.mapper;

import com.dy.domain.Order;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

public interface OrderMapper {
    /**
     * 查询所有
     *
     * @return ArrayList<Order>
     */
    @Select("select * from ord")
    ArrayList<Order> selectAll();

    /**
     * 通过oid查询Order
     *
     * @param oid 订单id
     * @return Order
     */
    @Select("select * from ord where oid  = #{oid}")
    Order selectByOid(int oid);

    /**
     * 通过uid查询Order
     *
     * @param uid 用户id
     * @return Order
     */
    @Select("select * from ord where uid  = #{uid}")
    ArrayList<Order> selectByUid(int uid);

    /**
     * 通过status查询Order
     *
     * @param status 申诉状态
     * @return Order
     */
    @Select("select * from ord where status  = #{status}")
    ArrayList<Order> selectByStatus(int status);

    /**
     * 添加order
     *
     * @param order 订单对象
     */
    void add(Order order);

    /**
     * 修改
     *
     * @param order 订单对象
     * @return 修改的行数
     */
    int update(Order order);

    /**
     * 根据id删除
     *
     * @param oid 要删除的订单id
     */
    void deleteById(int oid);


    /**
     * 批量删除
     *
     * @param ids 要删除的订单id数组
     */
    void deleteByIds(int[] ids);
}
