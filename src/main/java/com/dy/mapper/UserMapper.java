package com.dy.mapper;

import com.dy.domain.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

public interface UserMapper {
    /**
     * 登陆验证
     *
     * @param account  用户账号
     * @param password 密码
     * @return User类
     */
    @Select("select * from user where account  = #{account} and password = #{password}")
    User loginVerify(@Param("account") String account, @Param("password") String password);

    /**
     * 查询所有
     *
     * @return ArrayList<User>
     */
    @Select("select * from user")
    ArrayList<User> selectAll();

    /**
     * 通过id查询User
     *
     * @param id 用户id
     * @return User
     */
    @Select("select * from user where id  = #{id}")
    User selectById(int id);

    /**
     * 通过account查询User
     *
     * @param account 用户账号
     * @return User
     */
    @Select("select * from user where account  = #{account}")
    User selectByAccount(String account);

    /**
     * 条件查询
     *
     * @param user 用户对象
     * @return ArrayList<User>
     */
    ArrayList<User> selectByCondition(User user);

    /**
     * 添加user
     *
     * @param user 用户对象
     */
    void add(User user);

    /**
     * 修改
     *
     * @param user 用户对象
     * @return 修改的行数
     */
    int update(User user);

    /**
     * 根据id删除
     *
     * @param id 要删除的id
     */
    void deleteById(int id);


    /**
     * 批量删除
     *
     * @param ids 要删除的id数组
     */
    void deleteByIds(int[] ids);
}
