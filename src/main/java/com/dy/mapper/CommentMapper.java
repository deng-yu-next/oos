package com.dy.mapper;

import com.dy.domain.Comment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.ArrayList;

public interface CommentMapper {
    /**
     * 查询所有
     *
     * @return ArrayList<Comment>
     */
    @Select("select * from comment")
    ArrayList<Comment> selectAll();

    /**
     * 通过uid查询Comment
     *
     * @param uid 用户uid
     * @return ArrayList<Comment>
     */
    @Select("select * from comment where uid  = #{uid}")
    ArrayList<Comment> selectByUid(int uid);

    /**
     * 通过did查询Comment
     *
     * @param did 菜品did
     * @return ArrayList<Comment>
     */
    @Select("select * from comment where did  = #{did}")
    ArrayList<Comment> selectByDid(int did);

    /**
     * 添加Comment
     *
     * @param comment 评论对象
     */
    void add(Comment comment);

    /**
     * 修改
     *
     * @param comment 菜品对象
     * @return 修改的行数
     */
    int update(Comment comment);

    /**
     * 根据id删除
     * @param uid 用户uid
     * @param did 菜品did
     */
    void delete(@Param("uid") int uid, @Param("did") int did);

}
