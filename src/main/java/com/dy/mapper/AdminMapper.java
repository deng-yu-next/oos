package com.dy.mapper;

import com.dy.domain.Admin;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

public interface AdminMapper {
    /**
     * 登陆验证
     *
     * @param account  管理员账号
     * @param password 密码
     * @return Admin
     */
    @Select("select * from admin where account  = #{account} and password = #{password}")
    Admin loginVerify(@Param("account") String account, @Param("password") String password);

    /**
     * 查询所有
     *
     * @return ArrayList<Admin>
     */
    @Select("select * from admin")
    ArrayList<Admin> selectAll();

    /**
     * 通过id查询Admin
     *
     * @param id 管理员id
     * @return Admin
     */
    @Select("select * from admin where id  = #{id}")
    Admin selectById(int id);

    /**
     * 通过account查询Admin
     *
     * @param account 管理员账号
     * @return Admin
     */
    @Select("select * from admin where account  = #{account}")
    Admin selectByAccount(String account);

    /**
     * 添加admin
     *
     * @param admin 管理员对象
     */
    void add(Admin admin);

    /**
     * 修改
     *
     * @param admin 管理员对象
     * @return 修改的行数
     */
    int update(Admin admin);

    /**
     * 根据id删除
     *
     * @param id 要删除的id
     */
    void deleteById(int id);


    /**
     * 批量删除
     *
     * @param ids 要删除的id数组
     */
    void deleteByIds(int[] ids);
}
