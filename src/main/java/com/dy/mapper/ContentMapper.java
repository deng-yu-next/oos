package com.dy.mapper;

import com.dy.domain.Content;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.ArrayList;

public interface ContentMapper {
    /**
     * 查询所有
     *
     * @return ArrayList<Content>
     */
    @Select("select * from content")
    ArrayList<Content> selectAll();

    /**
     * 通过oid查询Content
     *
     * @param oid 订单oid
     * @return ArrayList<Content>
     */
    @Select("select * from content where oid  = #{oid}")
    ArrayList<Content> selectByOid(int oid);

    /**
     * 通过did查询Content
     *
     * @param did 菜品did
     * @return ArrayList<Content>
     */
    @Select("select * from content where did  = #{did}")
    ArrayList<Content> selectByDid(int did);

    /**
     * 添加Content
     *
     * @param content 订单内容对象
     */
    void add(Content content);

    /**
     * 修改
     *
     * @param content 订单内容对象
     * @return 修改的行数
     */
    int update(Content content);

    /**
     * 根据id删除
     * @param oid 订单oid
     * @param did 菜品did
     */
    void delete(@Param("oid") int oid, @Param("did") int did);

}
