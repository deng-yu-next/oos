package com.dy.mapper;

import com.dy.domain.Dishes;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

public interface DishesMapper {
    /**
     * 查询所有
     *
     * @return ArrayList<Dishes>
     */
    @Select("select * from dishes")
    ArrayList<Dishes> selectAll();

    /**
     * 通过did查询Dishes
     *
     * @param did 菜品id
     * @return Dishes 菜品对象
     */
    @Select("select * from dishes where did  = #{did}")
    Dishes selectById(int did);

    /**
     * 通过dname查询Dishes
     *
     * @param dname 菜品名
     * @return Dishes
     */
    @Select("select * from dishes where dname  = #{dname}")
    Dishes selectByDname(String dname);

    /**
     * 添加dishes
     *
     * @param dishes 菜品对象
     */
    void add(Dishes dishes);

    /**
     * 修改dishes
     *
     * @param dishes 菜品对象
     * @return 修改行数
     */
    int update(Dishes dishes);

    /**
     * 根据id删除
     * @param did 删除id
     */
    void deleteById(int did);


    /**
     * 批量删除
     * @param ids 要删除的id数组
     */
    void deleteByIds(int[] ids);

}
